/* ============================================================================================ */


-- Tabella contenente i dati dei team
CREATE TABLE team (
    team_id     SERIAL PRIMARY KEY,         -- ID progressivo
    team_name   TEXT NOT NULL               -- nome del team
);
-- Fornisco i privilegi necessari per il corretto funzionamento
GRANT ALL PRIVILEGES ON team TO "postgres";
-- Inserisco i valori di default nella tabella
INSERT INTO team VALUES (DEFAULT, 'team1'), (DEFAULT, 'team2');


/* ============================================================================================ */


-- Tabella contenente i dati degli utenti
CREATE TABLE userdata (
    user_id     SERIAL PRIMARY KEY,         -- ID progressivo
    username    TEXT NOT NULL,              -- username utente
    user_role   TEXT NOT NULL,              -- ruolo dell'utente [CEO, PM, DEV]
    team_id     INTEGER,                    -- team dell'utente [PM -> team che guida, DEV -> team di appartenenza]
    CONSTRAINT fk_team
        FOREIGN KEY(team_id)
        REFERENCES team(team_id)
);
-- Fornisco i privilegi necessari per il corretto funzionamento
GRANT ALL PRIVILEGES ON userdata TO "postgres";
-- Inserisco i valori di default nella tabella
INSERT INTO userdata VALUES (DEFAULT, 'CEO', 'CEO', NULL),
                            (DEFAULT, 'PM1', 'PM', 1),
                            (DEFAULT, 'PM2', 'PM', 2),
                            (DEFAULT, 'DEV1', 'DEV', 1),
                            (DEFAULT, 'DEV2', 'DEV', 1),
                            (DEFAULT, 'DEV3', 'DEV', 2),
                            (DEFAULT, 'DEV4', 'DEV', 2);


/* ============================================================================================ */


-- Tabella contenente i dati dei badge
CREATE TABLE badge (
    badge_id    SERIAL PRIMARY KEY,         -- ID progressivo
    user_badge  INTEGER NOT NULL,           -- utente associato al badge
    CONSTRAINT fk_user
        FOREIGN KEY(user_badge)
        REFERENCES userdata(user_id)
);
-- Fornisco i privilegi necessari per il corretto funzionamento
GRANT ALL PRIVILEGES ON badge TO "postgres";
-- Inserisco i valori di default nella tabella
INSERT INTO badge VALUES (DEFAULT, 1),
                         (DEFAULT, 2),
                         (DEFAULT, 3),
                         (DEFAULT, 4),
                         (DEFAULT, 5),
                         (DEFAULT, 6),
                         (DEFAULT, 7);


/* ============================================================================================ */


-- Tabella contenente gli accessi dei vari dipendenti in azienda
CREATE TABLE badgeevent (
    event_id            SERIAL PRIMARY KEY,         -- ID progressivo
    badge               INTEGER NOT NULL,           -- ID del badge che ha effettuato l'evento
    access_timestamp    TIMESTAMP NOT NULL,         -- timestamp dell'evento
    in_out_flag         CHAR NOT NULL,              -- flag tipologia evento [I = ingresso, O = uscita]
    CONSTRAINT fk_badge
        FOREIGN KEY(badge)
        REFERENCES badge(badge_id)
);
-- Fornisco i privilegi necessari per il corretto funzionamento
GRANT ALL PRIVILEGES ON badgeevent TO "postgres";
-- Inserisco i valori di default nella tabella
INSERT INTO badgeevent VALUES (DEFAULT, 1, '2022-07-12 08:30:00', 'I'),
                              (DEFAULT, 1, '2022-07-12 12:30:00', 'O'),
                              (DEFAULT, 1, '2022-07-12 14:00:00', 'I'),
                              (DEFAULT, 1, '2022-07-12 18:00:00', 'O');


/* ============================================================================================ */


-- Tabella contenente i progetti
CREATE TABLE project (
    project_id            SERIAL PRIMARY KEY,         -- ID progressivo
    project_manager       INTEGER NOT NULL,           -- PM del progetto
    project_name          TEXT NOT NULL,              -- nome del progetto
    CONSTRAINT fk_pm
        FOREIGN KEY(project_manager)
        REFERENCES userdata(user_id)
);
-- Fornisco i privilegi necessari per il corretto funzionamento
GRANT ALL PRIVILEGES ON project TO "postgres";
-- Inserisco i valori di default nella tabella
INSERT INTO project VALUES (DEFAULT, 2, 'progetto1'),
                           (DEFAULT, 3, 'progetto2');


/* ============================================================================================ */


-- Tabella contenente i task
CREATE TABLE task (
    task_id            SERIAL PRIMARY KEY,         -- ID progressivo
    task_desc          TEXT NOT NULL,              -- descrizione del task
    deadline           TIMESTAMP NOT NULL,         -- deadline del task
    project_id         INTEGER NOT NULL,           -- ID del progetto di riferimento del task
    CONSTRAINT fk_project
        FOREIGN KEY(project_id)
        REFERENCES project(project_id)
);
-- Fornisco i privilegi necessari per il corretto funzionamento
GRANT ALL PRIVILEGES ON task TO "postgres";
-- Inserisco i valori di default nella tabella
INSERT INTO task VALUES (DEFAULT, 'task1 progetto1', '2022-06-19 18:00:00', 1),
                        (DEFAULT, 'task2 progetto1', '2022-07-25 18:00:00', 1),
                        (DEFAULT, 'task1 progetto2', '2022-07-18 18:00:00', 2),
                        (DEFAULT, 'task2 progetto2', '2022-07-20 18:00:00', 2);


/* ============================================================================================ */


-- Tabella contenente i commit dei task
CREATE TABLE taskcommit (
    commit_id         SERIAL PRIMARY KEY,           -- ID progressivo
    user_id           INTEGER NOT NULL,             -- ID dell'utente che ha scritto il commit
    task_id           INTEGER NOT NULL,             -- ID del task di riferimento del commit
    text              TEXT NOT NULL,                -- testo del commit
    timestamp_commit  TIMESTAMP NOT NULL,           -- timestamp del commit
    CONSTRAINT fk_user
        FOREIGN KEY(user_id)
        REFERENCES userdata(user_id),
    CONSTRAINT fk_task
        FOREIGN KEY(task_id)
        REFERENCES task(task_id)
);
-- Fornisco i privilegi necessari per il corretto funzionamento
GRANT ALL PRIVILEGES ON taskcommit TO "postgres";
-- Inserisco i valori di default nella tabella
INSERT INTO taskcommit VALUES (DEFAULT, '4', '1', 'prova commit task 1', '2022-07-19 10:00:00'),
                              (DEFAULT, '4', '2', 'prova commit task 2', '2022-07-19 10:30:00');


/* ============================================================================================ */


-- Tabella contenente lo stato dei task [dev assegnato, stato task, timestamp stato, ...]
CREATE TABLE statustask (
    statustask_id     SERIAL PRIMARY KEY,           -- ID progressivo
    dev_id            INTEGER NOT NULL,             -- ID del DEV assegnato al task
    task_id           INTEGER NOT NULL,             -- ID del task di riferimento
    task_status       TEXT NOT NULL,                -- stato del task
    timestamp_status  TIMESTAMP NOT NULL,           -- timestamp dello stato del task
    CONSTRAINT fk_user
        FOREIGN KEY(dev_id)
            REFERENCES userdata(user_id),
    CONSTRAINT fk_task
        FOREIGN KEY(task_id)
            REFERENCES task(task_id)
);
-- Fornisco i privilegi necessari per il corretto funzionamento
GRANT ALL PRIVILEGES ON statustask TO "postgres";
-- Inserisco i valori di default nella tabella
INSERT INTO statustask VALUES (DEFAULT, 4, 1, 'In elaborazione', '2022-07-19 08:30:00'),
                              (DEFAULT, 5, 3, 'In elaborazione', '2022-07-19 08:30:00'),
                              (DEFAULT, 6, 1, 'In elaborazione', '2022-07-19 08:30:00');


/* ============================================================================================ */