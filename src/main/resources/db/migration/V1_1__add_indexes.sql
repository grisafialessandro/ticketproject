
/* ================================= INDICI TABELLA TEAM ================================= */

CREATE INDEX idx_team_teamName ON team(team_name);      -- Indice per nome team

/* ================================= INDICI TABELLA USERDATA ================================= */

CREATE INDEX idx_userdata_teamId ON userdata(team_id);                              -- Indice per ID team
CREATE INDEX idx_userdata_userIdAndUserRole ON userdata(user_id, user_role);        -- Indice per ID e ruolo utente

/* ================================= INDICI TABELLA BADGE ================================= */

CREATE INDEX idx_badge_userBadge ON badge(user_badge);      -- Indice per utente

/* ================================= INDICI TABELLA BADGEEVENT ================================= */

CREATE INDEX idx_badgeevent_badge ON badgeevent(badge);      -- Indice per badge

/* ================================= INDICI TABELLA PROJECT ================================= */

CREATE INDEX idx_project_pm ON project(project_manager);      -- Indice per PM

/* ================================= INDICI TABELLA TASK ================================= */

CREATE INDEX idx_task_project ON task(project_id);      -- Indice per progetto
CREATE INDEX idx_task_deadline ON task(deadline);       -- Indice per deadline

/* ================================= INDICI TABELLA TASKCOMMIT ================================= */

CREATE INDEX idx_taskcommit_userId ON taskcommit(user_id);      -- Indice per utente
CREATE INDEX idx_taskcommit_taskId ON taskcommit(task_id);      -- Indice per task

/* ================================= INDICI TABELLA STATUSTASK ================================= */

CREATE INDEX idx_statustask_devId ON statustask(dev_id);                            -- Indice per dev
CREATE INDEX idx_statustask_taskId ON statustask(task_id);                          -- Indice per task
CREATE INDEX idx_statustask_devIdAndTaskId ON statustask(dev_id, task_id);          -- Indice per dev e task
CREATE INDEX idx_statustask_devIdAndTaskStatus ON statustask(dev_id, task_status);  -- Indice per dev e stato task

