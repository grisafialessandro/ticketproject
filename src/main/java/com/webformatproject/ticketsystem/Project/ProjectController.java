package com.webformatproject.ticketsystem.Project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Classe in cui sono definiti i metodi esposti dall'API riguardanti i progetti.
 * Questa classe NON interagisce direttamente con il DB bensì fa da layer intermedio
 */
@RestController
@RequestMapping(path = "api/v1/project")
public class ProjectController {

    private final ProjectService projectService;  // Oggetto per interagire con il DB

    /**
     * Costruttore della classe
     *
     * @param projectService oggetto della classe per l'interazione con il DB
     *                       ATTENZIONE: attraverso l'annotazione @Autowired i parametri vengono automaticamente
     *                       inizializzati e iniettati all'interno del costruttore
     */
    @Autowired
    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    /**
     * Metodo per ottenere tutti i progetti presenti in DB
     *
     * @return lista contenente i progetti in DB
     */
    @GetMapping(path = "/getAllProjects")
    public List<Project> getAllProjects() {
        return this.projectService.getAllProjects();
    }

    /**
     * Metodo per ottenere tutti i progetti cross-team
     *
     * @return lista contenente i progetti cross-team
     */
    @GetMapping(path = "/getCrossTeamProjects")
    public List<Project> getCrossTeamProjects() {
        return this.projectService.getCrossTeamProjects();
    }

    /**
     * Metodo per aggiungere un nuovo progetto all'interno del DB
     *
     * @param project oggetto contenente i dati del progetto da inserire in DB [estratto dal body della richiesta]
     */
    @PostMapping(path = "/addProject", consumes = "application/json", produces = "application/json")
    public void addProject(@RequestBody Project project) {
        this.projectService.addProject(project);
    }
}
