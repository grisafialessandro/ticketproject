package com.webformatproject.ticketsystem.Project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Classe che modella un record di un progetto, per evitare codice boilerplate
 * i getter, setter, costruttori sono definiti tramite lombok
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "project")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "project_id")
    private Integer projectId;        // ID auto-generato del progetto

    @Column(name = "project_manager")
    private Integer projectManager;   // PM del progetto

    @Column(name = "project_name")
    private String projectName;      // Nome del progetto
}
