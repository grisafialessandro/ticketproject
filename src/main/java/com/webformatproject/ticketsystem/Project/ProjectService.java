package com.webformatproject.ticketsystem.Project;

import com.webformatproject.ticketsystem.User.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Classe in cui sono definiti i metodi per l'accesso/manipolazione dei dati dei progetti
 */
@Service
public class ProjectService {

    private final ProjectRepository projectRepository;  // Interfaccia dei metodi di accesso ai dati dei progetti
    private final UserRepository userRepository;        // Interfaccia dei metodi di accesso ai dati degli utenti

    /**
     * Costruttore della classe
     *
     * @param projectRepository interfaccia dei metodi di accesso ai dati dei progetti
     * @param userRepository    interfaccia dei metodi di accesso ai dati degli utenti
     *                          ATTENZIONE: attraverso l'annotazione @Autowired i parametri vengono automaticamente
     *                          inizializzati e iniettati all'interno del costruttore
     */
    @Autowired
    public ProjectService(ProjectRepository projectRepository,
                          UserRepository userRepository) {
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
    }

    /**
     * Metodo per ottenere tutti i progetti presenti in DB
     *
     * @return lista contenente i progetti in DB
     */
    public List<Project> getAllProjects() {
        // Per ottenere tutti i progetti viene utilizzato il metodo findAll offerto dall'interfaccia
        return this.projectRepository.findAll();
    }

    /**
     * Metodo per ottenere tutti i progetti cross-team
     *
     * @return lista contenente i progetti cross-team
     */
    public List<Project> getCrossTeamProjects() {
        return this.projectRepository.getCrossTeamProjects();
    }

    /**
     * Metodo per aggiungere un nuovo progetto all'interno del DB
     *
     * @param project oggetto contenente i dati del progetto da inserire in DB
     * @throws IllegalStateException se i dati non sono validi
     */
    public void addProject(Project project) {

        // Verifico che sia impostato il nome del progetto, altrimenti lancio un eccezione
        if (project.getProjectName() == null) {
            throw new IllegalStateException("Impossibile inserire il progetto. Il nome del progetto non è" +
                    " specificato");
        }

        // Verifico che non esista già un progetto con lo stesso nome, altrimenti lancio un eccezione
        if (this.projectRepository.existsProjectByProjectName(project.getProjectName())) {
            throw new IllegalStateException("Impossibile inserire il progetto. Esiste già un progetto con lo stesso" +
                    " nome");
        }

        // Verifico che il PM del progetto esista, altrimenti lancio un eccezione
        if (!this.userRepository.existsUserByUserIdAndUserRole(project.getProjectManager(), "PM")) {
            throw new IllegalStateException("Impossibile inserire il progetto. L'utente non esiste oppure" +
                    " non è un PM.");
        }

        // ATTENZIONE: a questo punto sono sicuro che i dati del progetto sono validi quindi procedo al salvataggio
        this.projectRepository.save(project);
    }
}
