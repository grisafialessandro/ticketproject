package com.webformatproject.ticketsystem.Project;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Interfaccia che estende JpaRepository per la definizione di metodi CRUD per la gestione dei dati dei progetti.
 * Di default dall'interfaccia JpaRepository vengono forniti dei metodi CRUD per le operazioni di base
 */
@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {

    /**
     * Metodo che verifica l'esistenza di un progetto dato il suo nome
     *
     * @param projectName nome del progetto di cui verificare l'esistenza
     * @return true se esiste, false se NON esiste
     */
    boolean existsProjectByProjectName(String projectName);

    /**
     * Metodo che ritorna tutti i progetti cross-team
     *
     * @return lista dei progetti cross-team
     */
    @Query(
            value = "SELECT * " +
                    "FROM project " +
                    "INNER JOIN ( " +
                    "SELECT DISTINCT(task.project_id) " +
                    "FROM task " +
                    "INNER JOIN statustask ON task.task_id = statustask.task_id " +
                    "INNER JOIN userdata ON statustask.dev_id = userdata.user_id " +
                    "GROUP BY task.project_id " +
                    "HAVING COUNT(*) > 1 " +
                    ") crossteamprojects ON crossteamprojects.project_id = project.project_id;",
            nativeQuery = true)
    List<Project> getCrossTeamProjects();
}
