package com.webformatproject.ticketsystem.Team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interfaccia che estende JpaRepository per la definizione di metodi CRUD per la gestione dei dati dei team.
 * Di default dall'interfaccia JpaRepository vengono forniti dei metodi CRUD per le operazioni di base
 */
@Repository
public interface TeamRepository extends JpaRepository<Team, Integer> {

    /**
     * Metodo che verifica l'esistenza di un team dato il suo nome
     *
     * @param teamName nome del team di cui verificare l'esistenza
     * @return true se esiste, false se NON esiste
     */
    boolean existsTeamByTeamName(String teamName);
}
