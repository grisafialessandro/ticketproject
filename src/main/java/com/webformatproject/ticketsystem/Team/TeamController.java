package com.webformatproject.ticketsystem.Team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Classe in cui sono definiti i metodi esposti dall'API riguardanti i team.
 * Questa classe NON interagisce direttamente con il DB bensì fa da layer intermedio
 */
@RestController
@RequestMapping(path = "api/v1/team")
public class TeamController {

    private final TeamService teamService;  // Oggetto per interagire con il DB

    /**
     * Costruttore della classe
     *
     * @param teamService oggetto della classe per l'interazione con il DB
     *                    ATTENZIONE: attraverso l'annotazione @Autowired i parametri vengono automaticamente
     *                    inizializzati e iniettati all'interno del costruttore
     */
    @Autowired
    public TeamController(TeamService teamService) {
        this.teamService = teamService;
    }

    /**
     * Metodo per ottenere tutti i team presenti in DB
     *
     * @return lista contenente i team in DB
     */
    @GetMapping(path = "/getAllTeams")
    public List<Team> getAllTeams() {
        return this.teamService.getAllTeams();
    }

    /**
     * Metodo per aggiungere un nuovo team all'interno del DB
     *
     * @param team oggetto contenente i dati del team da inserire in DB [estratto dal body della richiesta]
     */
    @PostMapping(path = "/addTeam", consumes = "application/json", produces = "application/json")
    public void addTeam(@RequestBody Team team) {
        this.teamService.addTeam(team);
    }
}