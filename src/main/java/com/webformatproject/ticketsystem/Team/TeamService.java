package com.webformatproject.ticketsystem.Team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Classe in cui sono definiti i metodi per l'accesso/manipolazione dei dati dei team
 */
@Service
public class TeamService {

    private final TeamRepository teamRepository;    // Interfaccia dei metodi di accesso ai dati dei team

    /**
     * Costruttore della classe
     *
     * @param teamRepository interfaccia dei metodi di accesso ai dati dei team
     *                       ATTENZIONE: attraverso l'annotazione @Autowired i parametri vengono automaticamente
     *                       inizializzati e iniettati all'interno del costruttore
     */
    @Autowired
    public TeamService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    /**
     * Metodo per ottenere tutti i team presenti in DB
     *
     * @return lista contenente i team in DB
     */
    public List<Team> getAllTeams() {
        // Per ottenere tutti i team viene utilizzato il metodo findAll offerto dall'interfaccia
        return this.teamRepository.findAll();
    }

    /**
     * Metodo per aggiungere un nuovo team all'interno del DB
     *
     * @param team oggetto contenente i dati del team da inserire in DB
     * @throws IllegalStateException se i dati non sono validi
     */
    public void addTeam(Team team) {

        // Verifico che il nome del team sia specificato, altrimenti lancio un eccezione
        if (team.getTeamName() == null) {
            throw new IllegalStateException("Impossibile inserire il team " + team.getTeamName() +
                    ". Nome del team non specificato.");
        }

        // Verifico che non esista un team con lo stesso nome, altrimenti lancio un eccezione
        if (this.teamRepository.existsTeamByTeamName(team.getTeamName())) {
            throw new IllegalStateException("Impossibile inserire il team " + team.getTeamName() +
                    ". Team già registrato.");
        }

        // ATTENZIONE: a questo punto sono sicuro che i dati del team sono validi quindi procedo al salvataggio
        this.teamRepository.save(team);
    }
}
