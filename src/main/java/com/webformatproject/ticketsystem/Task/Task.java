package com.webformatproject.ticketsystem.Task;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Classe che modella un record di un task, per evitare codice boilerplate
 * i getter, setter, costruttori sono definiti tramite lombok
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "task")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "task_id")
    private Integer taskId;             // ID auto-generato del task

    @Column(name = "task_desc")
    private String taskDesc;            // Descrizione del task

    @Column(name = "deadline")
    private ZonedDateTime deadline;     // Deadline del task

    @Column(name = "project_id")
    private Integer projectId;          // ID del progetto di riferimento del task

    /**
     * Metodo setter custom del campo deadline in maniera tale che l'oggetto venga popolato correttamente
     * dai JSON delle richieste
     *
     * @param deadline stringa contenente la deadline da impostare nell'oggetto
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    @JsonProperty("deadline")
    public void setDealine(String deadline) {
        // Parso e formatto la data all'interno dell'oggetto
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        this.deadline = ZonedDateTime.parse(deadline, formatter);
    }
}
