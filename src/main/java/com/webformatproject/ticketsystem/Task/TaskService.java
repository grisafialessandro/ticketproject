package com.webformatproject.ticketsystem.Task;

import com.webformatproject.ticketsystem.CustomBodyRequests.RespBodyTaskOverDeadline;
import com.webformatproject.ticketsystem.Project.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Classe in cui sono definiti i metodi per l'accesso/manipolazione dei dati dei task
 */
@Service
public class TaskService {

    private final TaskRepository taskRepository;        // Interfaccia dei metodi di accesso ai dati dei task
    private final ProjectRepository projectRepository;  // Interfaccia dei metodi di accesso ai dati dei progetti

    /**
     * Costruttore della classe
     *
     * @param taskRepository    interfaccia dei metodi di accesso ai dati dei task
     * @param projectRepository interfaccia dei metodi di accesso ai dati dei progetti
     *                          ATTENZIONE: attraverso l'annotazione @Autowired i parametri vengono automaticamente
     *                          inizializzati e iniettati all'interno del costruttore
     */
    @Autowired
    public TaskService(TaskRepository taskRepository,
                       ProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    /**
     * Metodo per ottenere tutti i task presenti in DB
     *
     * @return lista contenente i task in DB
     */
    public List<Task> getAllTasks() {
        // Per ottenere tutti i task viene utilizzato il metodo findAll offerto dall'interfaccia
        return this.taskRepository.findAll();
    }

    /**
     * Metodo per ottenere tutti in task "In elaborazione" di un DEV
     *
     * @param devId ID del DEV di cui ottenere i task "In elaborazione"
     * @return lista dei task "In elaborazione" del DEV specificato
     */
    public List<Task> getTaskInWorkDEV(Integer devId) {
        // Per ottenere tutti i task viene utilizzato il metodo findAll offerto dall'interfaccia
        return this.taskRepository.getTaskInWorkDEV(devId);
    }

    /**
     * Metodo per ottenere tutti in task, con relativi DEV e commit, che hanno sforato la deadline
     *
     * @return lista dei task, con relativi DEV e commit, che hanno sforato la deadline
     */
    public List<RespBodyTaskOverDeadline> getTaskOverDeadline() {
        return this.taskRepository.getTaskOverDeadline();
    }

    /**
     * Metodo per aggiungere un nuovo task all'interno del DB
     *
     * @param task oggetto contenente i dati del task da inserire in DB
     * @throws IllegalStateException se i dati non sono validi
     */
    public void addTask(Task task) {

        // Verifico che la deadline sia valorizzata, altrimenti lancio un eccezione
        if (task.getDeadline() == null) {
            throw new IllegalStateException("Impossibile inserire il task. La deadline non è specificata.");
        }

        // Verifico che il progetto di riferimento del task esista, altrimenti lancio un eccezione
        if (!this.projectRepository.existsById(task.getProjectId())) {
            throw new IllegalStateException("Impossibile inserire il task. Il progetto di riferimento non esiste.");
        }

        // ATTENZIONE: a questo punto sono sicuro che i dati del task sono validi quindi procedo al salvataggio
        this.taskRepository.save(task);
    }
}
