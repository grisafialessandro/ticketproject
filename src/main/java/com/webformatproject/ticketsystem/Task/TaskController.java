package com.webformatproject.ticketsystem.Task;

import com.webformatproject.ticketsystem.CustomBodyRequests.RespBodyTaskOverDeadline;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Classe in cui sono definiti i metodi esposti dall'API riguardanti i task.
 * Questa classe NON interagisce direttamente con il DB bensì fa da layer intermedio
 */
@RestController
@RequestMapping(path = "api/v1/task")
public class TaskController {

    private final TaskService taskService;  // Oggetto per interagire con il DB

    /**
     * Costruttore della classe
     *
     * @param taskService oggetto della classe per l'interazione con il DB
     *                    ATTENZIONE: attraverso l'annotazione @Autowired i parametri vengono automaticamente
     *                    inizializzati e iniettati all'interno del costruttore
     */
    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    /**
     * Metodo per ottenere tutti i task presenti in DB
     *
     * @return lista contenente i task in DB
     */
    @GetMapping(path = "/getAllTasks")
    public List<Task> getAllTasks() {
        return this.taskService.getAllTasks();
    }

    /**
     * Metodo per ottenere tutti in task "In elaborazione" di un DEV
     *
     * @param devId ID del DEV di cui ottenere i task "In elaborazione"
     * @return lista dei task "In elaborazione" del DEV specificato
     */
    @GetMapping(path = "/getTaskInWorkDEV", params = {"devId"})
    public List<Task> getTaskInWorkDEV(@RequestParam Integer devId) {
        return this.taskService.getTaskInWorkDEV(devId);
    }

    /**
     * Metodo per ottenere tutti in task, con relativi DEV e commit, che hanno sforato la deadline
     *
     * @return lista dei task, con relativi DEV e commit, che hanno sforato la deadline
     */
    @GetMapping(path = "/getTaskOverDeadline")
    public List<RespBodyTaskOverDeadline> getTaskOverDeadline() {
        return this.taskService.getTaskOverDeadline();
    }

    /**
     * Metodo per aggiungere un nuovo task all'interno del DB
     *
     * @param task oggetto contenente i dati del task da inserire in DB [estratto dal body della richiesta]
     */
    @PostMapping(path = "/addTask", consumes = "application/json", produces = "application/json")
    public void addTask(@RequestBody Task task) {
        this.taskService.addTask(task);
    }
}
