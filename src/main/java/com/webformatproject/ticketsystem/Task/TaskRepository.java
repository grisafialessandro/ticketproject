package com.webformatproject.ticketsystem.Task;

import com.webformatproject.ticketsystem.CustomBodyRequests.RespBodyTaskOverDeadline;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Interfaccia che estende JpaRepository per la definizione di metodi CRUD per la gestione dei dati dei task.
 * Di default dall'interfaccia JpaRepository vengono forniti dei metodi CRUD per le operazioni di base
 */
@Repository
public interface TaskRepository extends JpaRepository<Task, Integer> {

    /**
     * Metodo per ottenere tutti in task "In elaborazione" di un DEV
     *
     * @param devId ID del DEV di cui ottenere i task "In elaborazione"
     * @return lista dei task "In elaborazione" del DEV specificato
     */
    @Query(
            value = "SELECT task.* " +
                    "FROM task " +
                    "INNER JOIN " +
                    "(SELECT task_id FROM STATUSTASK WHERE task_status = 'In elaborazione' AND dev_id = ?1) statustak " +
                    "ON task.task_id = statustak.task_id;",
            nativeQuery = true)
    List<Task> getTaskInWorkDEV(Integer devId);

    /**
     * Metodo per ottenere tutti in task, con relativi DEV e commit, che hanno sforato la deadline
     *
     * @return lista dei task, con relativi DEV e commit, che hanno sforato la deadline
     */
    @Query(
            value = "SELECT task.task_id as taskId, " +
                    "taskcommit.commit_id as commitId, " +
                    "userdata.user_id as devId, " +
                    "task.task_desc as taskDesc, " +
                    "userdata.username, " +
                    "taskcommit.text as commitText " +
                    "FROM(SELECT task_id, task_desc FROM task WHERE deadline < now()) task " +
                    "INNER JOIN statustask ON task.task_id = statustask.task_id " +
                    "INNER JOIN userdata ON statustask.dev_id = userdata.user_id " +
                    "INNER JOIN taskcommit ON taskcommit.user_id = userdata.user_id AND taskcommit.task_id = task.task_id;",
            nativeQuery = true)
    List<RespBodyTaskOverDeadline> getTaskOverDeadline();
}
