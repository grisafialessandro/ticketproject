package com.webformatproject.ticketsystem.TaskCommit;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interfaccia che estende JpaRepository per la definizione di metodi CRUD per la gestione dei dati dei commit di un
 * task. Di default dall'interfaccia JpaRepository vengono forniti dei metodi CRUD per le operazioni di base
 */
@Repository
public interface TaskCommitRepository extends JpaRepository<TaskCommit, Integer> {
}
