package com.webformatproject.ticketsystem.TaskCommit;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;

/**
 * Classe che modella un record di un commit di un task, per evitare codice boilerplate
 * i getter, setter, costruttori sono definiti tramite lombok
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "taskcommit")
public class TaskCommit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "commit_id")
    private Integer commitId;                   // ID auto-generato del commit

    @Column(name = "user_id")
    private Integer userId;                     // ID dell'utente che ha scritto il commit

    @Column(name = "task_id")
    private Integer taskId;                     // ID del task di riferimento del commit

    @Column(name = "text")
    private String text;                        // Testo del commit

    @Column(name = "timestamp_commit")
    private ZonedDateTime timestampCommit;      // Timestamp del commit
}
