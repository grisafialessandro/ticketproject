package com.webformatproject.ticketsystem.TaskCommit;

import com.webformatproject.ticketsystem.Task.TaskRepository;
import com.webformatproject.ticketsystem.User.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Classe in cui sono definiti i metodi per l'accesso/manipolazione dei dati dei commit di un task
 */
@Service
public class TaskCommitService {

    // Interfaccia dei metodi di accesso ai dati dei commit di un task
    private final TaskCommitRepository taskCommitRepository;

    // Interfaccia dei metodi di accesso ai dati dei task
    private final TaskRepository taskRepository;

    // Interfaccia dei metodi di accesso ai dati degli utenti
    private final UserRepository userRepository;

    /**
     * Costruttore della classe
     *
     * @param taskCommitRepository interfaccia dei metodi di accesso ai dati dei commit di un task
     * @param taskRepository       interfaccia dei metodi di accesso ai dati dei task
     * @param userRepository       interfaccia dei metodi di accesso ai dati degli utenti
     *                             ATTENZIONE: attraverso l'annotazione @Autowired i parametri vengono automaticamente
     *                             inizializzati e iniettati all'interno del costruttore
     */
    @Autowired
    public TaskCommitService(TaskCommitRepository taskCommitRepository,
                             TaskRepository taskRepository,
                             UserRepository userRepository) {
        this.taskCommitRepository = taskCommitRepository;
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
    }

    /**
     * Metodo per ottenere tutti i commit dei task presenti in DB
     *
     * @return lista contenente i commit dei task in DB
     */
    public List<TaskCommit> getAllTaskCommits() {
        // Per ottenere tutti i commit viene utilizzato il metodo findAll offerto dall'interfaccia
        return this.taskCommitRepository.findAll();
    }

    /**
     * Metodo per aggiungere un nuovo commit di un task all'interno del DB
     *
     * @param taskCommit oggetto contenente i dati del commit di un task da inserire in DB
     * @throws IllegalStateException se i dati non sono validi
     */
    public void addTaskCommit(TaskCommit taskCommit) {

        // Verifico che il testo del commit sia specificato, altrimenti lancio un eccezione
        if (taskCommit.getText() == null) {
            throw new IllegalStateException("Impossibile inserire il commit. Il testo non è specificato.");
        }

        // Verifico che il task associato al task esista, altrimenti lancio un eccezione
        if (!this.taskRepository.existsById(taskCommit.getTaskId())) {
            throw new IllegalStateException("Impossibile inserire il commit. Il task associato non esiste.");
        }

        // Verifico che l'utente esista e che sia un DEV, altrimenti lancio un eccezione
        if (!this.userRepository.existsUserByUserIdAndUserRole(taskCommit.getUserId(), "DEV")) {
            throw new IllegalStateException("Impossibile inserire il commit. L'utente non esiste oppure non è " +
                    "un DEV.");
        }

        // ATTENZIONE: A questo punto sono sicuro che i dati del commit sono validi quindi imposto il timestamp attuale
        //             e procedo al salvataggio
        taskCommit.setTimestampCommit(ZonedDateTime.now());
        this.taskCommitRepository.save(taskCommit);
    }
}
