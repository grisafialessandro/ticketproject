package com.webformatproject.ticketsystem.TaskCommit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Classe in cui sono definiti i metodi esposti dall'API riguardanti i commit di un task.
 * Questa classe NON interagisce direttamente con il DB bensì fa da layer intermedio
 */
@RestController
@RequestMapping(path = "api/v1/taskcommit")
public class TaskCommitController {

    private final TaskCommitService taskCommitService;  // Oggetto per interagire con il DB

    /**
     * Costruttore della classe
     *
     * @param taskCommitService oggetto della classe per l'interazione con il DB
     *                          ATTENZIONE: attraverso l'annotazione @Autowired i parametri vengono automaticamente
     *                          inizializzati e iniettati all'interno del costruttore
     */
    @Autowired
    public TaskCommitController(TaskCommitService taskCommitService) {
        this.taskCommitService = taskCommitService;
    }

    /**
     * Metodo per ottenere tutti i commit dei task presenti in DB
     *
     * @return lista contenente i commit dei task in DB
     */
    @GetMapping(path = "/getAllTaskCommits")
    public List<TaskCommit> getAllTaskCommits() {
        return this.taskCommitService.getAllTaskCommits();
    }

    /**
     * Metodo per aggiungere un nuovo commit di un task all'interno del DB
     *
     * @param taskCommit oggetto contenente i dati del commit di un task da inserire in DB
     *                   [estratto dal body della richiesta]
     */
    @PostMapping(path = "/addTaskCommit", consumes = "application/json", produces = "application/json")
    public void addTaskCommit(@RequestBody TaskCommit taskCommit) {
        this.taskCommitService.addTaskCommit(taskCommit);
    }
}
