package com.webformatproject.ticketsystem.Badge;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Classe in cui sono definiti i metodi esposti dall'API riguardanti i badge.
 * Questa classe NON interagisce direttamente con il DB bensì fa da layer intermedio
 */
@RestController
@RequestMapping(path = "api/v1/badge")
public class BadgeController {

    private final BadgeService badgeService;  // Oggetto per interagire con il DB

    /**
     * Costruttore della classe
     *
     * @param badgeService oggetto della classe per l'interazione con il DB
     *                     ATTENZIONE: attraverso l'annotazione @Autowired i parametri vengono automaticamente
     *                     inizializzati e iniettati all'interno del costruttore
     */
    @Autowired
    public BadgeController(BadgeService badgeService) {
        this.badgeService = badgeService;
    }

    /**
     * Metodo per ottenere tutti i badge presenti in DB
     *
     * @return lista contenente i badge in DB
     */
    @GetMapping(path = "/getAllBadges")
    public List<Badge> getAllBadges() {
        return this.badgeService.getAllBadges();
    }

    /**
     * Metodo per aggiungere un nuovo badge all'interno del DB
     *
     * @param badge oggetto contenente i dati del badge da inserire in DB [estratto dal body della richiesta]
     */
    @PostMapping(path = "/addBadge", consumes = "application/json", produces = "application/json")
    public void addBadge(@RequestBody Badge badge) {
        this.badgeService.addBadge(badge);
    }
}
