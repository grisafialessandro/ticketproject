package com.webformatproject.ticketsystem.Badge;

import com.webformatproject.ticketsystem.User.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Classe in cui sono definiti i metodi per l'accesso/manipolazione dei dati dei badge
 */
@Service
public class BadgeService {

    private final BadgeRepository badgeRepository;    // Interfaccia dei metodi di accesso ai dati dei badge
    private final UserRepository userRepository;      // Interfaccia dei metodi di accesso ai dati degli utenti

    /**
     * Costruttore della classe
     *
     * @param badgeRepository interfaccia dei metodi di accesso ai dati dei badge
     * @param userRepository  interfaccia dei metodi di accesso ai dati degli utenti
     *                        ATTENZIONE: attraverso l'annotazione @Autowired i parametri vengono automaticamente
     *                        inizializzati e iniettati all'interno del costruttore
     */
    @Autowired
    public BadgeService(BadgeRepository badgeRepository,
                        UserRepository userRepository) {
        this.badgeRepository = badgeRepository;
        this.userRepository = userRepository;
    }

    /**
     * Metodo per ottenere tutti i badge presenti in DB
     *
     * @return lista contenente i badge in DB
     */
    public List<Badge> getAllBadges() {
        // Per ottenere tutti i badge viene utilizzato il metodo findAll offerto dall'interfaccia
        return this.badgeRepository.findAll();
    }

    /**
     * Metodo per aggiungere un nuovo badge all'interno del DB
     *
     * @param badge oggetto contenente i dati del badge da inserire in DB
     * @throws IllegalStateException se i dati non sono validi
     */
    public void addBadge(Badge badge) {

        // Verifico che l'utente associato al badge esista
        if (!this.userRepository.existsById(badge.getUserBadge())) {
            // Se non esiste lancio un eccezione
            throw new IllegalStateException("Impossibile inserire il badge, l'utente associato non esiste.");
        }

        // Verifico che non esista già un badge associato all'utente
        if (this.badgeRepository.existsByUserBadge(badge.getUserBadge())) {
            // Se esiste già un badge associato all'utente lancio un eccezione
            throw new IllegalStateException("Impossibile inserire il badge, esiste già un badge associato " +
                    "all'utente " + badge.getUserBadge() + ".");
        }

        // ATTENZIONE: a questo punto sono sicuro che i dati del badge sono validi quindi procedo al salvataggio
        this.badgeRepository.save(badge);
    }
}
