package com.webformatproject.ticketsystem.Badge;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Classe che modella un record di un badge, per evitare codice boilerplate
 * i getter, setter, costruttori sono definiti tramite lombok
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "badge")
public class Badge {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "badge_id")
    private Integer badgeId;        // ID auto-generato del badge

    @Column(name = "user_badge")
    private Integer userBadge;      // Utente associato al badge
}
