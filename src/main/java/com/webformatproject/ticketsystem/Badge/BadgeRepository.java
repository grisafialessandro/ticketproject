package com.webformatproject.ticketsystem.Badge;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interfaccia che estende JpaRepository per la definizione di metodi CRUD per la gestione dei dati dei badge.
 * Di default dall'interfaccia JpaRepository vengono forniti dei metodi CRUD per le operazioni di base
 */
@Repository
public interface BadgeRepository extends JpaRepository<Badge, Integer> {

    /**
     * Metodo che verifica l'esistenza di un badge per l'utente specificato
     *
     * @param userId ID dell'utente di cui verificare l'esistenza del badge
     * @return true se esiste, false se NON esiste
     */
    boolean existsByUserBadge(Integer userId);
}
