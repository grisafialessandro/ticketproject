package com.webformatproject.ticketsystem.StatusTask;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interfaccia che estende JpaRepository per la definizione di metodi CRUD per la gestione dei dati dello stato dei
 * task. Di default dall'interfaccia JpaRepository vengono forniti dei metodi CRUD per le operazioni di base
 */
@Repository
public interface StatusTaskRepository extends JpaRepository<StatusTask, Integer> {

    /**
     * Metodo che verifica se il DEV ha il task assegnato [se esiste almeno un record vuol dire che gli è stato
     * assegnato]
     *
     * @param devId  ID del DEV
     * @param taskId ID del task del quale verificare se è già assegnato al DEV
     * @return true se già assegnato, false se NON assegnato
     */
    boolean existsByDevIdAndTaskId(Integer devId, Integer taskId);

    /**
     * Metodo che rimuove un task di un particolare DEV
     *
     * @param devId  ID del DEV da rimuovere dal task
     * @param taskId ID del task da rimuovere al DEV
     */
    void deleteAllByDevIdAndTaskId(Integer devId, Integer taskId);
}
