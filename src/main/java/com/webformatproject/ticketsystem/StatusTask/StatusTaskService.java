package com.webformatproject.ticketsystem.StatusTask;

import com.webformatproject.ticketsystem.CustomBodyRequests.ReqBodyTaskDev;
import com.webformatproject.ticketsystem.Task.TaskRepository;
import com.webformatproject.ticketsystem.User.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * Classe in cui sono definiti i metodi per l'accesso/manipolazione dei dati dello stato dei task
 */
@Service
public class StatusTaskService {

    // Interfaccia dei metodi di accesso ai dati dello stato dei task
    private final StatusTaskRepository statusTaskRepository;
    // Interfaccia dei metodi di accesso ai dati dei task
    private final TaskRepository taskRepository;
    // Interfaccia dei metodi di accesso ai dati degli utenti
    private final UserRepository userRepository;

    /**
     * Costruttore della classe
     *
     * @param statusTaskRepository interfaccia dei metodi di accesso ai dati dello stato dei task
     * @param taskRepository       interfaccia dei metodi di accesso ai dati dei task
     * @param userRepository       interfaccia dei metodi di accesso ai dati degli utenti
     *                             ATTENZIONE: attraverso l'annotazione @Autowired i parametri vengono automaticamente
     *                             inizializzati e iniettati all'interno del costruttore
     */
    @Autowired
    public StatusTaskService(StatusTaskRepository statusTaskRepository,
                             TaskRepository taskRepository,
                             UserRepository userRepository) {
        this.statusTaskRepository = statusTaskRepository;
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
    }

    /**
     * Metodo per ottenere tutti gli stati dei task presenti in DB
     *
     * @return lista contenente gli stati dei task in DB
     */
    public List<StatusTask> getAllStatusTasks() {
        // Per ottenere tutti gli stati dei task viene utilizzato il metodo findAll offerto dall'interfaccia
        return this.statusTaskRepository.findAll();
    }

    /**
     * Metodo per assegnare un task a un DEV
     *
     * @param reqBodyTaskDev oggetto contenente i dati necessari ad assegnare il task al DEV
     * @throws IllegalStateException se i dati non sono validi
     */
    public void assignTaskToDev(ReqBodyTaskDev reqBodyTaskDev) {

        // Verifico che il DEV esista, altrimenti lancio un eccezione
        if (!this.userRepository.existsUserByUserIdAndUserRole(reqBodyTaskDev.getDevId(), "DEV")) {
            throw new IllegalStateException("Impossibile assegnare il task al DEV. L'utente non esiste oppure" +
                    " non è un DEV.");
        }

        // Verifico che il task esista, altrimenti lancio un eccezione
        if (!this.taskRepository.existsById(reqBodyTaskDev.getTaskId())) {
            throw new IllegalStateException("Impossibile assegnare il task al DEV. Il task non esiste.");
        }

        // Verifico che il task non sia già assegnato allo stesso DEV, altrimenti lancio un eccezione
        if (this.statusTaskRepository.existsByDevIdAndTaskId(
                reqBodyTaskDev.getDevId(),
                reqBodyTaskDev.getTaskId())) {
            throw new IllegalStateException("Impossibile assegnare il task al DEV. Il task è già assegnato allo" +
                    " stesso DEV.");
        }

        // ATTENZIONE: a questo punto sono sicuro che i dati sono validi quindi procedo al salvataggio
        this.statusTaskRepository.save(new StatusTask(
                reqBodyTaskDev.getDevId(),
                reqBodyTaskDev.getTaskId(),
                "Assegnato",
                ZonedDateTime.now()));
    }

    /**
     * Metodo per rimuovere un task a un DEV
     *
     * @param reqBodyTaskDev oggetto contenente i dati necessari a rimuovere il task al DEV
     * @throws IllegalStateException se i dati non sono validi
     */
    @Transactional
    public void deleteTaskDev(ReqBodyTaskDev reqBodyTaskDev) {

        // Verifico se il task è assegnato al DEV, altrimenti lancio un eccezione
        if (!this.statusTaskRepository.existsByDevIdAndTaskId(
                reqBodyTaskDev.getDevId(),
                reqBodyTaskDev.getTaskId())) {
            throw new IllegalStateException("Impossibile rimuovere il task al DEV. Il task non è assegnato al DEV.");
        }

        // ATTENZIONE: a questo punto sono sicuro che i dati sono validi quindi procedo a rimuovere tutti i record
        //             del task legati al DEV
        this.statusTaskRepository.deleteAllByDevIdAndTaskId(
                reqBodyTaskDev.getDevId(),
                reqBodyTaskDev.getTaskId());
    }
}
