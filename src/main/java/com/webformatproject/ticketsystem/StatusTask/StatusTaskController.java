package com.webformatproject.ticketsystem.StatusTask;

import com.webformatproject.ticketsystem.CustomBodyRequests.ReqBodyTaskDev;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Classe in cui sono definiti i metodi esposti dall'API riguardanti lo stato dei task.
 * Questa classe NON interagisce direttamente con il DB bensì fa da layer intermedio
 */
@RestController
@RequestMapping(path = "api/v1/statustask")
public class StatusTaskController {

    private final StatusTaskService statusTaskService;  // Oggetto per interagire con il DB

    /**
     * Costruttore della classe
     *
     * @param statusTaskService oggetto della classe per l'interazione con il DB
     *                          ATTENZIONE: attraverso l'annotazione @Autowired i parametri vengono automaticamente
     *                          inizializzati e iniettati all'interno del costruttore
     */
    @Autowired
    public StatusTaskController(StatusTaskService statusTaskService) {
        this.statusTaskService = statusTaskService;
    }

    /**
     * Metodo per ottenere tutti gli stati dei task presenti in DB
     *
     * @return lista contenente gli stati dei task in DB
     */
    @GetMapping(path = "/getAllStatusTasks")
    public List<StatusTask> getAllStatusTasks() {
        return this.statusTaskService.getAllStatusTasks();
    }

    /**
     * Metodo per assegnare un task a un DEV
     *
     * @param reqBodyTaskDev oggetto contenente i dati necessari ad assegnare il task al DEV
     */
    @PostMapping(path = "/assignTaskToDev", consumes = "application/json", produces = "application/json")
    public void assignTaskToDev(@RequestBody ReqBodyTaskDev reqBodyTaskDev) {
        this.statusTaskService.assignTaskToDev(reqBodyTaskDev);
    }

    /**
     * Metodo per rimuovere un task a un DEV
     *
     * @param reqBodyTaskDev oggetto contenente i dati necessari a rimuovere il task al DEV
     */
    @DeleteMapping(path = "deleteTaskDev", consumes = "application/json", produces = "application/json")
    public void deleteTaskDev(@RequestBody ReqBodyTaskDev reqBodyTaskDev) {
        this.statusTaskService.deleteTaskDev(reqBodyTaskDev);
    }
}
