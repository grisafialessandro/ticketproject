package com.webformatproject.ticketsystem.StatusTask;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;

/**
 * Classe che modella un record dello stato di un task, per evitare codice boilerplate
 * i getter, setter, costruttori sono definiti tramite lombok
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "statustask")
public class StatusTask {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "statustask_id")
    private Integer statustaskId;           // ID auto-generato dello stato del task

    @Column(name = "dev_id")
    private Integer devId;                  // ID del DEV assegnato al task

    @Column(name = "task_id")
    private Integer taskId;                 // ID del task di riferimento

    @Column(name = "task_status")
    private String taskStatus;              // Stato del task

    @Column(name = "timestamp_status")
    private ZonedDateTime timestampStatus;  // Timestamp dello stato del task

    /**
     * Costruttore custom senza statustaskId in quanto verrà popolato dal automaticamente dal DB
     *
     * @param devId           ID del DEV assegnato al task
     * @param taskId          ID del task di riferimento
     * @param taskStatus      Stato del task
     * @param timestampStatus Timestamp dello stato del task
     */
    public StatusTask(Integer devId,
                      Integer taskId,
                      String taskStatus,
                      ZonedDateTime timestampStatus) {
        this.devId = devId;
        this.taskId = taskId;
        this.taskStatus = taskStatus;
        this.timestampStatus = timestampStatus;
    }
}
