package com.webformatproject.ticketsystem.CustomBodyRequests;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Classe che modella il JSON delle richieste che riguardano un task e un DEV inviato nel body, per evitare
 * codice boilerplate i getter, setter, costruttori sono definiti tramite lombok
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReqBodyTaskDev {
    private Integer devId;      // ID del DEV di riferimento del task
    private Integer taskId;     // ID del task di riferimento del DEV
}
