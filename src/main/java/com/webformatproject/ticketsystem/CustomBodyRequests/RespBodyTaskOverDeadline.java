package com.webformatproject.ticketsystem.CustomBodyRequests;

/**
 * Interfaccia che fornisce i metodi GET dei campi del JSON della riposta della richiesta dei task che hanno superato
 * la deadline. Spring costruirà automaticamente l'oggetto al momento per ogni riga del ResultSet della query
 */
public interface RespBodyTaskOverDeadline {
    Integer getTaskID();        // Metodo GET dell'ID del task

    Integer getCommitId();      // Metodo GET dell'ID dei commit del task

    Integer getDevId();         // Metodo GET dell'ID dei DEV del task

    String getTaskDesc();       // Metodo GET della descrizione del task

    String getUsername();       // Metodo GET del nome utente del DEV

    String getCommitText();     // Metodo GET del testo del commit
}