package com.webformatproject.ticketsystem.CustomBodyRequests;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Classe che modella il JSON delle richieste che riguardano un team e un DEV inviato nel body, per evitare
 * codice boilerplate i getter, setter, costruttori sono definiti tramite lombok
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReqBodyTeamDev {
    private Integer devId;      // ID del DEV a cui cambiare team
    private Integer teamId;     // ID del team da assegnare al DEV
}
