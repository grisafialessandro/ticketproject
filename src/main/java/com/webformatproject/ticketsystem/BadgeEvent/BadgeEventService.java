package com.webformatproject.ticketsystem.BadgeEvent;

import com.webformatproject.ticketsystem.Badge.BadgeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Classe in cui sono definiti i metodi per l'accesso/manipolazione dei dati degli eventi dei badge
 */
@Service
public class BadgeEventService {

    // Interfaccia dei metodi di accesso ai dati degli eventi scaturiti dai badge
    private final BadgeEventRepository badgeEventRepository;

    // Interfaccia dei metodi di accesso ai dati dei badge
    private final BadgeRepository badgeRepository;

    /**
     * Costruttore della classe
     *
     * @param badgeEventRepository interfaccia dei metodi di accesso ai dati degli eventi scaturiti dai badge
     * @param badgeRepository      Interfaccia dei metodi di accesso ai dati dei badge
     *                             ATTENZIONE: attraverso l'annotazione @Autowired i parametri vengono automaticamente
     *                             inizializzati e iniettati all'interno del costruttore
     */
    @Autowired
    public BadgeEventService(BadgeEventRepository badgeEventRepository,
                             BadgeRepository badgeRepository) {
        this.badgeEventRepository = badgeEventRepository;
        this.badgeRepository = badgeRepository;
    }

    /**
     * Metodo per ottenere tutti gli eventi dei badge presenti in DB
     *
     * @return lista contenente gli eventi dei badge in DB
     */
    public List<BadgeEvent> getAllBadgesEvents() {
        // Per ottenere tutti gli eventi dei badge viene utilizzato il metodo findAll offerto dall'interfaccia
        return this.badgeEventRepository.findAll();
    }

    /**
     * Metodo per aggiungere un nuovo evento di un badge in DB
     *
     * @param badgeEvent oggetto contenente i dati dell'evento di un badge [estratto dal body della richiesta]
     * @throws IllegalStateException se i dati non sono validi
     */
    public void addBadgeEvent(BadgeEvent badgeEvent) {

        // Verifico che il badge che ha scaturito l'evento esista, altrimenti lancio un eccezione
        if (!this.badgeRepository.existsById(badgeEvent.getBadge())) {
            throw new IllegalStateException("Impossibile inserire l'evento per il badge, il badge non esiste.");
        }

        // Verifico che il flag della tipologia di evento sia impostato, altrimenti lancio un eccezione
        if (badgeEvent.getInOutFlag() == null) {
            throw new IllegalStateException("Impossibile inserire l'evento per il badge, non è stato specificata" +
                    " la tipologia dell'evento");
        }

        // Verifico che il flag della tipologia di evento abbia un valore valido, altrimenti lancio un eccezione
        if (badgeEvent.getInOutFlag() != 'I' && badgeEvent.getInOutFlag() != 'O') {
            throw new IllegalStateException("Impossibile inserire l'evento per il badge, la tipologia dell'evento" +
                    " è sconosciuta");
        }

        // ATTENZIONE: A questo punto sono sicuro che dati dell'evento sono validi quindi imposto il timestamp attuale
        //              e procedo al salvataggio
        badgeEvent.setAccessTimestamp(ZonedDateTime.now());
        this.badgeEventRepository.save(badgeEvent);
    }
}
