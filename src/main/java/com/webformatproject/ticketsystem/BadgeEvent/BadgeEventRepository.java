package com.webformatproject.ticketsystem.BadgeEvent;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interfaccia che estende JpaRepository per la definizione di metodi CRUD per la gestione dei dati degli eventi
 * scaturiti dai badge. Di default dall'interfaccia JpaRepository vengono forniti dei metodi
 * CRUD per le operazioni di base
 */
@Repository
public interface BadgeEventRepository extends JpaRepository<BadgeEvent, Integer> {
}
