package com.webformatproject.ticketsystem.BadgeEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Classe in cui sono definiti i metodi esposti dall'API riguardanti gli eventi dei badge.
 * Questa classe NON interagisce direttamente con il DB bensì fa da layer intermedio
 */
@RestController
@RequestMapping(path = "api/v1/badgeevent")
public class BadgeEventController {

    private final BadgeEventService badgeEventService;  // Oggetto per interagire con il DB

    /**
     * Costruttore della classe
     *
     * @param badgeEventService oggetto della classe per l'interazione con il DB
     *                          ATTENZIONE: attraverso l'annotazione @Autowired i parametri vengono automaticamente
     *                          inizializzati e iniettati all'interno del costruttore
     */
    @Autowired
    public BadgeEventController(BadgeEventService badgeEventService) {
        this.badgeEventService = badgeEventService;
    }

    /**
     * Metodo per ottenere tutti gli eventi dei badge presenti in DB
     *
     * @return lista contenente gli eventi dei badge in DB
     */
    @GetMapping(path = "/getAllBadgesEvents")
    public List<BadgeEvent> getAllBadgesEvents() {
        return this.badgeEventService.getAllBadgesEvents();
    }

    /**
     * Metodo per aggiungere un nuovo evento di un badge in DB
     *
     * @param badgeEvent oggetto contenente i dati dell'evento di un badge [estratto dal body della richiesta]
     */
    @PostMapping(path = "/addBadgeEvent", consumes = "application/json", produces = "application/json")
    public void addTeam(@RequestBody BadgeEvent badgeEvent) {
        this.badgeEventService.addBadgeEvent(badgeEvent);
    }
}
