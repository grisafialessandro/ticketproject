package com.webformatproject.ticketsystem.BadgeEvent;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;

/**
 * Classe che modella un record di un evento di un badge, per evitare codice boilerplate
 * i getter, setter, costruttori sono definiti tramite lombok
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "badgeevent")
public class BadgeEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "event_id")
    private Integer eventId;                    // ID auto-generato dell'evento

    @Column(name = "badge")
    private Integer badge;                      // ID del badge che ha effettuato l'evento

    @Column(name = "access_timestamp")
    private ZonedDateTime accessTimestamp;      // Timestamp dell'evento

    @Column(name = "in_out_flag")
    private Character inOutFlag;                // Flag tipologia evento [I = ingresso, O = uscita]
}
