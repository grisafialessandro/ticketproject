package com.webformatproject.ticketsystem.User;

import com.webformatproject.ticketsystem.CustomBodyRequests.ReqBodyTeamDev;
import com.webformatproject.ticketsystem.Team.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

/**
 * Classe in cui sono definiti i metodi per l'accesso/manipolazione dei dati degli utenti
 */
@Service
public class UserService {

    private final UserRepository userRepository;    // Interfaccia dei metodi di accesso ai dati degli utenti
    private final TeamRepository teamRepository;    // Interfaccia dei metodi di accesso ai dati dei team

    /**
     * Costruttore della classe
     *
     * @param userRepository interfaccia dei metodi di accesso ai dati degli utenti
     * @param teamRepository interfaccia dei metodi di accesso ai dati dei team
     *                       ATTENZIONE: attraverso l'annotazione @Autowired i parametri vengono automaticamente
     *                       inizializzati e iniettati all'interno del costruttore
     */
    @Autowired
    public UserService(UserRepository userRepository,
                       TeamRepository teamRepository) {
        this.userRepository = userRepository;
        this.teamRepository = teamRepository;
    }

    /**
     * Metodo per ottenere tutti gli utenti presenti in DB
     *
     * @return lista contenente gli utenti in DB
     */
    public List<User> getAllUsers() {
        // Per ottenere tutti gli utenti viene utilizzato il metodo findAll offerto dall'interfaccia
        return this.userRepository.findAll();
    }

    /**
     * Metodo per ottenere il PM di riferimento di un DEV
     *
     * @param devId ID del DEV di cui ottenere le informazioni del PM di riferimento
     * @return oggetto popolato con le informazioni del PM di riferimento del DEV
     * @throws IllegalStateException se i dati non sono validi
     */
    public User getPMDev(Integer devId) {

        // Ottengo le informazioni del DEV se esistente, altrimenti lancio un eccezione
        User devData = this.userRepository.findUserByUserIdAndUserRole(devId, "DEV")
                .orElseThrow(() -> new IllegalStateException("Impossibile ottenere il PM di riferimento per l'utente: "
                        + devId + ". L'utente non esiste oppure non è un DEV."));

        // Se il DEV esiste verifico che sia all'interno di un team, altrimenti lancio un eccezione
        // ATTENZIONE: non eseguito all'interno della query per specializzare di più l'errore ritornato
        if (devData.getTeamId() == null) {
            throw new IllegalStateException("Impossibile ottenere il PM di riferimento per il DEV: " +
                    devId + ". Il DEV non appartiene ad un team.");
        }

        // Ottengo i dati del PM del team del DEV se esistente, altrimenti lancio un eccezione
        return this.userRepository.findUserByUserRoleAndTeamId("PM", devData.getTeamId())
                .orElseThrow(() -> new IllegalStateException("Impossibile ottenere il PM di riferimento per il DEV: " +
                        devId + ". Il PM non esiste."));
    }

    /**
     * Metodo per aggiungere un nuovo utente all'interno del DB
     *
     * @param user oggetto contenente i dati dell'utente da inserire in DB
     * @throws IllegalStateException se i dati non sono validi
     */
    public void addUser(User user) {

        // Verifico che il nome utente sia specificato, altrimenti lancio un eccezione
        if (user.getUsername() == null) {
            throw new IllegalStateException("Impossibile inserire l'utente, username non specificato.");
        }

        // Verifico che il ruolo dell'utente sia specificato, altrimenti lancio un eccezione
        if (user.getUserRole() == null) {
            throw new IllegalStateException("Impossibile inserire l'utente, ruolo non specificato.");
        }

        // Verifico che non esista un utente con lo stesso username, altrimenti lancio un eccezione
        if (this.userRepository.existsUserByUsername(user.getUsername())) {
            throw new IllegalStateException("Impossibile inserire l'utente " + user.getUsername() +
                    ". Username già registrato.");
        }

        // A seconda del ruolo dell'utente effettuo verifiche differenti
        if (Objects.equals(user.getUserRole(), "CEO")) {

            // --- Controllo correttezza dati specifici per il CEO

            // Verifico che non esista già un utente CEO, altrimenti lancio un eccezione
            if (this.userRepository.existsUserByUserRole("CEO")) {
                throw new IllegalStateException("Impossibile inserire l'utente " + user.getUsername() +
                        ". Esiste già un CEO.");
            }

            // Verifico che non sia stato assegnato un team al CEO, altrimenti lancio un eccezione
            if (user.getTeamId() != null) {
                throw new IllegalStateException("Impossibile inserire l'utente " + user.getUsername() +
                        ". Al CEO non può essere assegnato un team.");
            }
        } else if (Objects.equals(user.getUserRole(), "PM")) {

            // --- Controllo correttezza dati specifici per i PM

            // Verifico se il PM ha un team affidato
            if (user.getTeamId() != null) {
                // Verifico che il suo team NON sia già affidato da un altro PM, altrimenti lancio un eccezione
                if (this.userRepository.existsUserByUserRoleAndTeamId("PM", user.getTeamId())) {
                    throw new IllegalStateException("Impossibile inserire l'utente " + user.getUsername() +
                            ". Esiste già un PM a cui è affidato il team: " + user.getTeamId() + ".");
                }
            }
        }

        // ATTENZIONE: a questo punto sono sicuro che i dati dell'utente sono corretti quindi procedo al salvataggio
        this.userRepository.save(user);
    }

    /**
     * Metodo per assegnare un team ad un DEV
     *
     * @param reqBodyTeamDev oggetto contenente i dati per assegnare il team al DEV
     * @throws IllegalStateException se i dati non sono validi
     */
    @Transactional
    public void assignTeamDev(ReqBodyTeamDev reqBodyTeamDev) {

        // Ottengo i dati del DEV se esistente, altrimenti lancio un eccezione
        User dev = this.userRepository.findUserByUserIdAndUserRole(reqBodyTeamDev.getDevId(), "DEV")
                .orElseThrow(() -> new IllegalStateException("Impossibile assegnare il team al DEV. L'utente" +
                        " non esiste oppure non è un DEV."));

        // Verifico l'esistenza del team da assegnare al DEV
        if (!this.teamRepository.existsById(reqBodyTeamDev.getTeamId())) {
            throw new IllegalStateException("Impossibile assegnare il team al DEV. Il team non esiste");
        }

        // ATTENZIONE: a questo punto sono sicuro che i dati siano corretti quindi procedo ad aggiornare il team del DEV
        dev.setTeamId(reqBodyTeamDev.getTeamId());
    }
}
