package com.webformatproject.ticketsystem.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Interfaccia che estende JpaRepository per la definizione di metodi CRUD per la gestione dei dati degli utenti.
 * Di default dall'interfaccia JpaRepository vengono forniti dei metodi CRUD per le operazioni di base
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    /**
     * Metodo per verificare se esiste un utente dato il suo username
     *
     * @param username username dell'utente del quale verificare l'esistenza
     * @return true se esiste, false se NON esiste
     */
    boolean existsUserByUsername(String username);

    /**
     * Metodo per verificare se esiste un utente di un determinato ruolo
     *
     * @param userRole ruolo dell'utente del quale verificare l'esistenza del'utente
     * @return true se esiste, false se NON esiste
     */
    boolean existsUserByUserRole(String userRole);

    /**
     * Metodo per verificare se esiste un utente dati il suo ruolo e il suo team
     *
     * @param userRole ruolo dell'utente del quale verificare l'esistenza
     * @param teamId   ID del team dell'utente del quale verificare l'esistenza
     * @return true se esiste, false se NON esiste
     */
    boolean existsUserByUserRoleAndTeamId(String userRole, Integer teamId);

    /**
     * Metodo per verificare se esiste un utente dati il suo ID e il suo ruolo
     *
     * @param userId   ID dell'utente del quale verificare l'esistenza
     * @param userRole ruolo dell'utente del quale verificare l'esistenza
     * @return true se esiste, false se NON esiste
     */
    boolean existsUserByUserIdAndUserRole(Integer userId, String userRole);

    /**
     * Metodo per ottenere le informazioni di un utente dati il suo ruolo e il suo team
     *
     * @param userRole ruolo dell'utente del quale recuperare i dati
     * @param teamId   ID del team dell'utente del quale recuperare i dati
     * @return oggetto contente i dati dell'utente se esistente
     */
    Optional<User> findUserByUserRoleAndTeamId(String userRole, Integer teamId);

    /**
     * Metodo per ottenere le informazioni di un utente dati il suo ID e il suo ruolo
     *
     * @param userId   ID dell'utente del quale recuperare i dati
     * @param userRole ruolo dell'utente del quale recuperare i dati
     * @return oggetto contente i dati dell'utente se esistente
     */
    Optional<User> findUserByUserIdAndUserRole(Integer userId, String userRole);
}
