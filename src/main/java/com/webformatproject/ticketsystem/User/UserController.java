package com.webformatproject.ticketsystem.User;

import com.webformatproject.ticketsystem.CustomBodyRequests.ReqBodyTeamDev;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Classe in cui sono definiti i metodi esposti dall'API riguardanti gli utenti.
 * Questa classe NON interagisce direttamente con il DB bensì fa da layer intermedio
 */
@RestController
@RequestMapping(path = "api/v1/user")
public class UserController {

    private final UserService userService;  // Oggetto per interagire con il DB

    /**
     * Costruttore della classe
     *
     * @param userService oggetto della classe per l'interazione con il DB
     *                    ATTENZIONE: attraverso l'annotazione @Autowired i parametri vengono automaticamente
     *                    inizializzati e iniettati all'interno del costruttore
     */
    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Metodo per ottenere tutti gli utenti presenti in DB
     *
     * @return lista contenente gli utenti in DB
     */
    @GetMapping(path = "/getAllUsers")
    public List<User> getAllUsers() {
        return this.userService.getAllUsers();
    }

    /**
     * Metodo per ottenere il PM di riferimento di un DEV
     *
     * @param devId ID del DEV di cui ottenere le informazioni del PM di riferimento
     * @return oggetto popolato con le informazioni del PM di riferimento del DEV
     */
    @GetMapping(path = "/getPMDev")
    public User getPMDev(@RequestParam Integer devId) {
        return this.userService.getPMDev(devId);
    }

    /**
     * Metodo per aggiungere un nuovo utente all'interno del DB
     *
     * @param user oggetto contenente i dati dell'utente da inserire in DB [estratto dal body della richiesta]
     */
    @PostMapping(path = "/addUser", consumes = "application/json", produces = "application/json")
    public void addUser(@RequestBody User user) {
        this.userService.addUser(user);
    }

    /**
     * Metodo per assegnare un team ad un DEV
     *
     * @param reqBodyTeamDev oggetto contenente i dati per assegnare il team al DEV [estratto dal body della richiesta]
     */
    @PutMapping(path = "/assignTeamDev", consumes = "application/json", produces = "application/json")
    public void assignTeamDev(@RequestBody ReqBodyTeamDev reqBodyTeamDev) {
        this.userService.assignTeamDev(reqBodyTeamDev);
    }
}
