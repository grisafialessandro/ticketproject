package com.webformatproject.ticketsystem.User;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import javax.persistence.*;

/**
 * Classe che modella un record di un utente, per evitare codice boilerplate
 * i getter, setter, costruttori sono definiti tramite lombok
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "userdata")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer userId;         // ID auto-generato dell'utente
    @Column(name = "username")
    private String username;        // Username dell'utente
    @Column(name = "user_role")
    private String userRole;        // Ruolo dell'utente [CEO, PM, DEV]
    @Column(name = "team_id")
    private Integer teamId;         // Team dell'utente

}
